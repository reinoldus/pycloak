import json

import requests
import pycloak.auth


class Users:
    """
    Manages Users within a specific realm.
    """

    KEYCLOAK_API_URL="{0}/auth/admin/realms/{1}/users"

    def __init__(self, auth_session, realm):
        self.auth_session = auth_session
        self.realm = realm

    def get_users(self):
        """
        Returns list of users

        :return:
        """
        api_url = self.KEYCLOAK_API_URL.format(self.auth_session.host, self.realm)
        # https://sso.logarithmo.de/auth/admin/realms/AppStore-Production/users
        client_response = requests.get(api_url, headers=self.auth_session.bearer_header)

        if client_response.status_code != 200:
            raise Exception(
                "Could not get the user for the following realm: {}".format(self.realm))

        return [
            User(self.auth_session, self.realm, user_json) for user_json in client_response.json()
        ]


class User:
    KEYCLOAK_API_URL="{0}/auth/admin/realms/{1}/users/{2}"

    def __init__(self, auth_session, realm, user_dict: dict):
        self.user = user_dict
        self.auth_session = auth_session
        self.realm = realm

    def add_attributes(self, attributes):
        current_attributes = self.get_attributes()

        # Extend or update values
        current_attributes.update(attributes)

        api_url = self.KEYCLOAK_API_URL.format(self.auth_session.host, self.realm, self.user["id"])
        client_response = requests.put(api_url, json=self.user, headers=self.auth_session.bearer_header)

        if client_response.status_code != 204:
            raise Exception(
                "Could not upate the attributes for the user with the following id: {}".format(self.user["id"]))

        return True

    def get_attributes(self):
        """
        Returns the users attributes, empty dict if there are none

        :return:
        """

        if "attributes" not in self.user:
            # This is important, so the empty dict we return is the same as the one in this class
            # Do not improve with this: self.user.get("attributes", {}) !
            self.user["attributes"] = {}

        return self.user["attributes"]


# if __name__ == "__main__":
#     a_s = pycloak.auth.AuthSession('admin', 'password', host="http://localhost:8080")
#     u = Users(a_s, "master")
#     us = u.get_users()
#     print(us)
#     us[0].add_attributes({"x": ["I INSERTED THIS", "xx"]})
#     us = u.get_users()
#     print(us[0].get_attributes())
