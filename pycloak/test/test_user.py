from pycloak import auth, admin, realm, client
import pytest

from pycloak.users import Users


def test_get_users(keycloak_server, admin_username, admin_password):
    session = auth.AuthSession(admin_username, admin_password)

    users = Users(session, "master")
    users_list = users.get_users()

    assert len(users_list) == 1


def test_add_user_attribute(keycloak_server, admin_username, admin_password):
    session = auth.AuthSession(admin_username, admin_password)

    users = Users(session, "master")
    user_list = users.get_users()

    test_key = "test"
    test_val = "this is a specific test"

    assert user_list[0].add_attributes({
        test_key: test_val
    })

    # This repulls the users from the server
    user_list = users.get_users()
    assert test_key in user_list[0].get_attributes()

    # For some reason keycloak returns the value as a list
    assert user_list[0].get_attributes()[test_key] == [test_val]